# pybingo.py

## creates a set of bingo boards from a configuration file


## Installation:

pip install --upgrade -r requirements.txt

install the package 'wkhtmltopdf' using your distribution package manager:

sudo apt install wkhtmltopdf

pamac install wkhtmltopdf

## usage: python pybingo.py 

## configuration:

in config.toml set number of desired boards and the size of each board (n x n)

if n is odd, the center space will be a "free" space, otherwise the board will be completely filled

set your items in items.txt, one desired item per line. Make sure your number of items in the file is greater than or equal to the size of your board

run python pybingo.py and the resulting boards will be in boards/ or as html pages in html/

a combined pdf will be in merged.pdf
