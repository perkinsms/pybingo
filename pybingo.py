from random import shuffle, sample
from more_itertools import grouper
import toml
from jinja2 import Template
import pdfkit
import PyPDF2


with open('template.html.jinja2', encoding='utf-8') as f:
    jinja_template = Template(f.read())

config = toml.load('config.toml')
num_cards = config['num_cards']
size_card = config['size_card']

options = {
        "enable-local-file-access": None,
        "resolve-relative-links": None,
        "page-size": "Letter",
        }


with open("items.txt", encoding="utf-8") as f:
    # item_list contains all the items that can be in a bingo box
    item_list = [item.strip() for item in f.readlines()]

if size_card % 2 == 0:
    # even number on a side
    # it's size^2. -1 because it goes from 0 to n^2-1 instead of 1 to n
    num_spaces = size_card * size_card - 1
else:
    # odd number on a side
    # similar to even numbers n^2-1
    # free space index is in the middle (floor divide)
    num_spaces = size_card * size_card - 1
    free_space_index = num_spaces//2

pdf_merger = PyPDF2.PdfMerger()

for i in range(num_cards):
    # item_list_copy contains a sample of the items for each board to be generated)
    item_list_copy = sample(item_list, k=num_spaces)
    shuffle(item_list_copy)
    if not size_card % 2 == 0:
        item_list_copy.insert(free_space_index, "Free<br>Space")

    rows = []

    for item in list(grouper(item_list_copy, size_card)):
        rows.append(list(item))

    with open(f"html/output {i}.html", 'w', encoding="utf-8") as f:
        # the jinja template produces the table with rows and items in rows
        f.write(jinja_template.render(rows=rows))

    print(f"html/output {i}.html")
    print(f"boards/output {i}.pdf")

    # create a pdf from the html output
    pdfkit.from_file(f"html/output {i}.html", f"boards/output {i}.pdf", options=options)

    with open(f"boards/output {i}.pdf", 'rb') as pdf_file:
        pdf_reader = PyPDF2.PdfReader(pdf_file)
        pdf_merger.append(pdf_reader)

pdf_merger.write('merged.pdf')

